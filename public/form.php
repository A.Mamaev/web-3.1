<html lang="ru">
  <head>
      <title>Задание 3 -  Мамаев</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
      <link href="style.css" type="text/css" rel="stylesheet">
  </head>
  <body>
    <form id="form" action="" method="POST">
        <!-- Текстовое поле для имени -->
        <div class="field">
            <label class="label" for="name-input">Имя</label>
            <div class="control">
                <input id="name-input" name="name" class="input is-info" type="text" placeholder="Ваше имя">
            </div>
        </div>
        <!-- Текстовое поле для почты -->
        <div class="field">
            <label for="form-email" class="label">Email</label>
            <div class="control">
                <input id="form-email" name="email" class="input is-info" type="email" placeholder="Ваш email">
            </div>
        </div>
        <!-- Выбор из списка для года рождения -->
        <div class="field">
            <label for="age-select" class="label">Год рождения</label>
            <div class="control">
                <div class="select is-info">
                    <select name="year" id="age-select">
                      <?php
                        // Пробегаем от 2014 до 1965 и вставляем option
                        for ($i = 2014; $i > 1965; $i--) {
                          print('<option value="'.$i.'">'.$i.'</option>');
                        }
                       ?>
                    </select>
                </div>
            </div>
        </div>
        <!-- Чекбокс -->
        <div class="field">
            <div class="control">
                <label class="checkbox">
                    <input type="checkbox" name="check"/>
                    С <a href="#" class="has-text-info">контрактом</a> ознакомлен(а).
                </label>
            </div>
        </div>
        <!-- Кнопка отправить -->
        <div class="control">
            <button id="btn" type="submit" class="button is-light is-info">Отправить</button>
        </div>
    </form>
  </body>
</html>